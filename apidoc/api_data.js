define({ "api": [
  {
    "type": "get",
    "url": "/teams",
    "title": "1 Request all teams",
    "version": "1.0.0",
    "name": "AllTeams",
    "group": "Teams",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "0",
            "description": "<p>Team Object.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "0.id",
            "description": "<p>Id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "0.name",
            "description": "<p>Name</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success 200 Example",
          "content": " HTTP/1.1 200 OK\n[\n    {\n        \"id\": 1,\n       \"name\": \"Backend\"\n    },\n    {\n        \"id\": 2,\n       \"name\": \"IOS\"\n    },\n    {\n        \"id\": 3,\n       \"name\": \"Android\"\n    },\n    {\n        \"id\": 4,\n       \"name\": \"Xamarin\"\n    },\n    {\n        \"id\": 5,\n       \"name\": \"QA\"\n    },\n    {\n        \"id\": 6,\n       \"name\": \"Frontend\"\n    }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "ServerError 500": [
          {
            "group": "ServerError 500",
            "type": "string",
            "optional": false,
            "field": "error",
            "description": "<p>Server error.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ServerError 500 Example",
          "content": "  HTTP/1.1 404 Not Found\n{\n    error: Server error. Try again.\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Exam/TeamController.php",
    "groupTitle": "Teams"
  },
  {
    "type": "delete",
    "url": "/teams/:id",
    "title": "5 Delete a specific  team",
    "version": "1.0.0",
    "name": "DeleteTeam",
    "group": "Teams",
    "parameter": {
      "fields": {
        "Url params": [
          {
            "group": "Url params",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Team unique id.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success 204 Example",
          "content": "HTTP/1.1 204 OK",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "EntityNotFound 404": [
          {
            "group": "EntityNotFound 404",
            "type": "string",
            "optional": false,
            "field": "error",
            "description": "<p>The id of the team was not found.</p>"
          }
        ],
        "ServerError 500": [
          {
            "group": "ServerError 500",
            "type": "string",
            "optional": false,
            "field": "error",
            "description": "<p>Server error.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "EntityNotFound 404 Example",
          "content": "  HTTP/1.1 404 Not Found\n{\n    error: Entity not found\n}",
          "type": "json"
        },
        {
          "title": "ServerError 500 Example",
          "content": "  HTTP/1.1 404 Not Found\n{\n    error: Server error. Try again.\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Exam/TeamController.php",
    "groupTitle": "Teams"
  },
  {
    "type": "get",
    "url": "/teams/:id",
    "title": "2 Request a specific team",
    "version": "1.0.0",
    "name": "GetTeam",
    "group": "Teams",
    "parameter": {
      "fields": {
        "Url params": [
          {
            "group": "Url params",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Team unique id.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name</p>"
          },
          {
            "group": "Success 200",
            "type": "Array[]",
            "optional": false,
            "field": "members",
            "description": "<p>Members</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "members.0",
            "description": "<p>Member Object</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "members.0.id",
            "description": "<p>Member id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "members.0.name",
            "description": "<p>Member name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "members.0.email",
            "description": "<p>Member email</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "members.0.image",
            "description": "<p>Member image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "members.0.team_id",
            "description": "<p>Member team_id</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "members.1",
            "description": "<p>Member Object</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "members.1.id",
            "description": "<p>Member id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "members.1.name",
            "description": "<p>Member name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "members.1.email",
            "description": "<p>Member email</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "members.1.image",
            "description": "<p>Member image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "members.1.team_id",
            "description": "<p>Member team_id</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "members.2",
            "description": "<p>Member Object</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "members.2.id",
            "description": "<p>Member id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "members.2.name",
            "description": "<p>Member name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "members.2.email",
            "description": "<p>Member email</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "members.2.image",
            "description": "<p>Member image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "members.2.team_id",
            "description": "<p>Member team_id</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "members.3",
            "description": "<p>Member Object</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "members.3.id",
            "description": "<p>Member id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "members.3.name",
            "description": "<p>Member name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "members.3.email",
            "description": "<p>Member email</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "members.3.image",
            "description": "<p>Member image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "members.3.team_id",
            "description": "<p>Member team_id</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success 200 Example",
          "content": " HTTP/1.1 200 OK\n{\n   \"id\": 1,\n   \"name\": \"Backend\",\n   \"members\": [\n     {\n       \"id\": 1,\n       \"name\": \"Madelyn Nolan\",\n       \"email\": \"bins.karen@example.org\",\n       \"image\": \"YniiDUkdiuIs.jpg\",\n       \"team_id\": \"1\"\n     },\n     {\n       \"id\": 2,\n       \"name\": \"Irving Pfeffer\",\n       \"email\": \"crona.sasha@example.org\",\n       \"image\": \"Ghdfddfso324.jpg\",\n       \"team_id\": \"1\"\n     },\n     {\n       \"id\": 3,\n       \"name\": \"Prof. Edwardo Parker\",\n       \"email\": \"sabryna.goyette@example.org\",\n       \"image\": \"YniieeDUkdiuIs.jpg\",\n       \"team_id\": \"1\"\n     },\n     {\n       \"id\": 4,\n       \"name\": \"Mr. Roman Purdy\",\n       \"email\": \"granville25@example.org\",\n       \"image\": 345df3ddf.jpg\",\n       \"team_id\": \"1\"\n     },\n     {\n       \"id\": 5,\n       \"name\": \"Shemar Kohler\",\n       \"email\": \"jgibson@example.org\",\n       \"image\": \"YniiDdfaiuIs.jpg\",\n       \"team_id\": \"1\"\n     }\n   ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "EntityNotFound 404": [
          {
            "group": "EntityNotFound 404",
            "type": "string",
            "optional": false,
            "field": "error",
            "description": "<p>The id of the team was not found.</p>"
          }
        ],
        "ServerError 500": [
          {
            "group": "ServerError 500",
            "type": "string",
            "optional": false,
            "field": "error",
            "description": "<p>Server error.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "EntityNotFound 404 Example",
          "content": "  HTTP/1.1 404 Not Found\n{\n    error: Entity not found\n}",
          "type": "json"
        },
        {
          "title": "ServerError 500 Example",
          "content": "  HTTP/1.1 404 Not Found\n{\n    error: Server error. Try again.\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Exam/TeamController.php",
    "groupTitle": "Teams"
  },
  {
    "type": "post",
    "url": "/teams",
    "title": "3 Store a  team",
    "version": "1.0.0",
    "name": "StoreTeam",
    "group": "Teams",
    "parameter": {
      "fields": {
        "FormData": [
          {
            "group": "FormData",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Team name.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 201": [
          {
            "group": "Success 201",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Id.</p>"
          },
          {
            "group": "Success 201",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success 201 Example",
          "content": " HTTP/1.1 201 OK\n{\n    id: 1,\n    name : \"Infra\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "ValidationErrors 400": [
          {
            "group": "ValidationErrors 400",
            "type": "array[]",
            "optional": false,
            "field": "name",
            "description": "<p>List of errors for name field.</p>"
          },
          {
            "group": "ValidationErrors 400",
            "type": "string",
            "optional": false,
            "field": "name.0",
            "description": "<p>First error for name.</p>"
          },
          {
            "group": "ValidationErrors 400",
            "type": "string",
            "optional": false,
            "field": "name.1",
            "description": "<p>Second error for name.</p>"
          }
        ],
        "ServerError 500": [
          {
            "group": "ServerError 500",
            "type": "string",
            "optional": false,
            "field": "error",
            "description": "<p>Server error.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ValidationErrors 400 Example",
          "content": "  HTTP/1.1 400 Bad Request\n{\n    name: [\n         The name field is required\n    ]\n}",
          "type": "json"
        },
        {
          "title": "ServerError 500 Example",
          "content": "  HTTP/1.1 404 Not Found\n{\n    error: Server error. Try again.\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Exam/TeamController.php",
    "groupTitle": "Teams"
  },
  {
    "type": "put",
    "url": "/teams/:id",
    "title": "4 Update a specific  team",
    "version": "1.0.0",
    "name": "UpdateTeam",
    "group": "Teams",
    "parameter": {
      "fields": {
        "Url params": [
          {
            "group": "Url params",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Team unique id.</p>"
          }
        ],
        "FormData": [
          {
            "group": "FormData",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Team name.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success 200 Example",
          "content": " HTTP/1.1 201 OK\n{\n    id: 1,\n    name : \"Infra\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "EntityNotFound 404": [
          {
            "group": "EntityNotFound 404",
            "type": "string",
            "optional": false,
            "field": "error",
            "description": "<p>The id of the team was not found.</p>"
          }
        ],
        "ValidationErrors 400": [
          {
            "group": "ValidationErrors 400",
            "type": "array[]",
            "optional": false,
            "field": "name",
            "description": "<p>List of errors for name field.</p>"
          },
          {
            "group": "ValidationErrors 400",
            "type": "string",
            "optional": false,
            "field": "name.0",
            "description": "<p>First error for name.</p>"
          },
          {
            "group": "ValidationErrors 400",
            "type": "string",
            "optional": false,
            "field": "name.1",
            "description": "<p>Second error for name.</p>"
          }
        ],
        "ServerError 500": [
          {
            "group": "ServerError 500",
            "type": "string",
            "optional": false,
            "field": "error",
            "description": "<p>Server error.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "EntityNotFound 404 Example",
          "content": "  HTTP/1.1 404 Not Found\n{\n    error: Entity not found\n}",
          "type": "json"
        },
        {
          "title": "ValidationErrors 400 Example",
          "content": "  HTTP/1.1 400 Bad Request\n{\n    name: [\n         The name field is required\n    ]\n}",
          "type": "json"
        },
        {
          "title": "ServerError 500 Example",
          "content": "  HTTP/1.1 404 Not Found\n{\n    error: Server error. Try again.\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Exam/TeamController.php",
    "groupTitle": "Teams"
  }
] });
