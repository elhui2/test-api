<?php

/**
 * Api
 * Controlador del API
 * @package Codeigniter 3.1.3
 * @version Version 0.1
 * @author Daniel Huidobro <daniel.hui287@gmail.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->helper('url');
        
        redirect('/apidoc');
    }

    /**
     * teams
     * @return json con la respueste de la solicitud
     * Consulta rest de los equipos
     */
    public function teams() {
        $this->load->model('teams_tb');

        $teams = $this->teams_tb->get_teams();

        if (!$teams) {
            return $this->json_response(204, Array('No existen registros con tu busqueda'));
        }

        if ($teams == 'error') {
            return $this->json_response(500, Array('Ocurrio un error en el servidor, intenta mas tarde'));
        }

        return $this->json_response(200, $teams);
    }

    /**
     * team
     * @return json con la respueste de la solicitud
     * Consulta rest de un equipo completo
     */
    public function team($idTeam = FALSE) {
        if (!$idTeam) {
            return $this->json_response(401, array('Parametros incompletos'));
        }

        $this->load->model('teams_tb');

        $team = $this->teams_tb->get_team($idTeam);

        if (!$team) {
            return $this->json_response(404, Array('No existen registros con tu busqueda'));
        }

        if ($team == 'error') {
            return $this->json_response(500, Array('Ocurrio un error en el servidor, intenta mas tarde'));
        }

        return $this->json_response(200, $team);
    }

    /**
     * new_team
     * @return json con la respueste de la solicitud
     * Crea un nuevo equipo
     */
    public function new_team() {
        if (!$this->input->post('name')) {
            return $this->json_response(401, array('Parametros incompletos'));
        }

        if (strlen($this->input->post('name')) < 4) {
            return $this->json_response(403, array('La longitud del nombre debe ser de al menus 4 caracteres'));
        }

        $this->load->model('teams_tb');

        $dataTeam = array(
            'name' => $this->input->post('name'),
            'created_at' => date('Y-m-d h:i')
        );

        $team = $this->teams_tb->create($dataTeam);
        if ($team) {
            unset($team['created_at'], $team['updated_at']);
            return $this->json_response(201, $team);
        } else {
            return $this->json_response(500, array('Ocurrio un error en el servidor, intenta mas tarde'));
        }
    }

    /**
     * update_team
     * @return json con la respueste de la solicitud
     * Actualiza un equipos
     */
    public function update_team() {

        if (!$_REQUEST['id'] || !$_REQUEST['name'] || empty($_REQUEST['name']) || empty($_REQUEST['id'])) {
            return $this->json_response(401, array('Parametros incompletos'));
        }

        if (strlen($_REQUEST['name']) < 4) {
            return $this->json_response(403, array('La longitud del nombre debe ser de al menus 4 caracteres'));
        }

        $this->load->model('teams_tb');

        $team = $this->teams_tb->read($_REQUEST['id']);

        if (!$team) {
            return $this->json_response(404, array('No existe registro'));
        }

        $dataTeam = array(
            'name' => $_REQUEST['name'],
            'updated_at' => date('Y-m-d h:i')
        );

        $team = $this->teams_tb->update($dataTeam, $_REQUEST['id']);
        if ($team) {
            unset($team['created_at'], $team['updated_at']);
            return $this->json_response(202, $team);
        } else {
            return $this->json_response(500, array('Ocurrio un error en el servidor, intenta mas tarde'));
        }
    }

    /**
     * delete_team
     * @return json con la respueste de la solicitud
     * Elimina un equipo
     */
    public function delete_team() {

        if (!$_REQUEST['id'] || empty($_REQUEST['id'])) {
            return $this->json_response(401, array('Parametros incompletos'));
        }

        $this->load->model('teams_tb');
        $team = $this->teams_tb->read($_REQUEST['id']);

        if (!$team) {
            return $this->json_response(404, array('No existe registro'));
        }

        $team = $this->teams_tb->delete($_REQUEST['id']);
        if ($team) {
            return $this->json_response(202, array('OK'));
        } else {
            return $this->json_response(500, array('Ocurrio un error en el servidor, intenta mas tarde'));
        }
    }

    /**
     * json_response
     * Todas las repuestas en json
     * @param int $code Codigo http de la transaccion
     * @param array $response Datos de la transaccion
     * @return type
     */
    protected function json_response($code, $response) {

        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header($code)
                        ->set_output(json_encode($response));
    }

}
