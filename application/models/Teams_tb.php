<?php

/**
 * Teams_tb
 * Modelo de la tabla teams
 * @package Codeigniter 3.1.3
 * @version Version 0.1
 * @author Daniel Huidobro <daniel.hui287@gmail.com>
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Teams_tb extends CI_Model {

    /**
     * env
     * Caracteristicas importantes de la tabla
     * "name" => "Nombre de la Tabla"
     * @var array 
     */
    protected $env;

    /**
     * @version string
     */
    public function __construct() {
        parent::__construct();
        $this->env = array(
            'name' => 'teams'
        );
    }

    /**
     * create
     * Inserta un registro en la tabla
     * @param array $dataRegister Array asociativo con los datos del registro
     * @return mixed array con el registro o false
     */
    public function create($dataRegister) {
        if ($this->db->insert($this->env['name'], $dataRegister)) {
            $idTeam = $this->db->insert_id();
            $team = $this->read($idTeam);
            return $team;
        } else {
            return FALSE;
        }
    }

    /**
     * read
     * Lee un registro de la tabla
     * @param type $idRegister Llave primaria del registro en la tabla
     * @return mixed array con el registro o false
     */
    public function read($idRegister) {
        $this->db->where('id', $idRegister);
        $result = $this->db->get($this->env['name']);
        return $result->result_array()[0];
    }

    /**
     * update
     * @param array $dataRegister Array asociativo con los datos del registro
     * @param int $idRegister Llave primaria del registro en la tabla
     */
    public function update($dataRegister, $idRegister) {
        $this->db->where('id', $idRegister);
        if ($this->db->update($this->env['name'], $dataRegister)) {
            return $this->read($idRegister);
        } else {
            return FALSE;
        }
    }

    /**
     * delete
     * Elimina un registro de la tabla
     * @param type $idRegister Llave primaria del registro en la tabla
     * @return boolean
     */
    public function delete($idRegister) {
        $this->db->where('id', $idRegister);
        if ($this->db->delete($this->env['name'])) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * get_teams
     * @return array Array con todos los equipos
     * Obtiene los equipos
     */
    public function get_teams() {

        $this->db->select('id,name');
        $result = $this->db->get('teams');

        if ($this->db->error()['code']) {
            return 'error';
        }

        if ($result->num_rows()) {
            return $result->result_array();
        } else {
            return FALSE;
        }
    }

    /**
     * get_team
     * @param int $idTeam ID del equipo
     * @return array Datos del equipo, falso si no existe
     * Obtiene los datos de un equipo de teams y de members
     */
    public function get_team($idTeam) {
        $team = $this->read($idTeam);

        if (!$team) {
            return FALSE;
        }

        $this->db->select('id,name,email,image,team_id');
        $this->db->where('team_id', $idTeam);
        $result = $this->db->get('members');

        if ($this->db->error()['code']) {
            return 'error';
        }

        $members = $result->result_array();

        $response = array(
            'id' => $team['id'],
            'name' => $team['name'],
            'members' => $members
        );

        return $response;
    }

}
